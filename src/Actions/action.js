/*
 * action types
 */

export const ADD_PHOTO = 'ADD_PHOTO'


/*
 * action creators
 */

export function addPhoto(text) {
  return { type: ADD_PHOTO, text }
}

// store to get state of one photo when we make action of "add on wishlist"



import {addPhoto} from './Actions/action'
import { createStore } from 'redux';
import wishApp from './Reducers/reducers';


const store=createStore(wishApp);

export function storeFunction  (photo) {	
const unsubscribe = store.subscribe(() =>
  console.log (store.getState())
)

// Dispatch some actions
store.dispatch(addPhoto(photo))

    

// Stop listening to state updates
unsubscribe();
}
export function getPhoto (){
	return store.getState();
}

 
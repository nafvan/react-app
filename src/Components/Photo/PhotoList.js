
import React, {Component} from "react";
import  Photo from "./Photo";
import {isoFetch} from '../../Actions/fetch';
import { Link } from 'react-router-dom';

/*
PhotoList component contained by list of photos. Thoses photos are 
collected by API
*/
 	class PhotoList extends Component{
  constructor( props) {

      super(props)
        this.state = {
      	dataPhoto:[]

      }
   
  }

  componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/photos')
   
    response
       .then(dataPhoto =>{this.setState({dataPhoto}); 
       	console.log(dataPhoto)})
       .catch(reason => console.error(reason.message))
       
      
  }


  render() {

    const {dataPhoto}=this.state;
    const {albumId}=this.state;
    // we get albumId in url by this.props.match.params.userId
    this.state.albumId=parseInt(this.props.match.params.userId);
    // we use filter to get photos which have the albumId on URL
    let tableau=this.state.dataPhoto.filter( element=>
    element.albumId==this.props.match.params.userId )

    return (
      <div className="photo-List"><h2>Photolist</h2>
           <div class="row">
              {
                tableau.map((photo)=>(
              	   <div class="col-sm-3 thumbnail " key={photo.userId}>
                    <Link to={`/albums/${photo.albumId}/${photo.id}`}>
                      <img  src={photo.thumbnailUrl} alt={photo.title} /></Link>
                  </div>
              ))}
          </div>
      </div>);}



}

export default PhotoList;
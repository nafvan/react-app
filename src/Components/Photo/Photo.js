

import React, {Component} from "react";
import PropTypes from "prop-types";
import Wishlist from '../Wishlist/Wishlist.js';
import {isoFetch} from '../../Actions/fetch';	
import {storeFunction} from '../../store';
import '../../css/photo.css';

/* 
  Photo component contained by header, contain and footer
  We have button to add photo in wishlist
  we send the photo by storeFunction()
  */
class Photo extends Component
{
	
 	constructor(props){
 		super(props)
 		this.state = {
      	dataPhoto:[]

      }

   
 	}
 	 handleClick(e, photo) {
        storeFunction(photo);
    
    }

 	 componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/photos')
   
    response
       .then(dataPhoto =>{this.setState({dataPhoto})})
       .catch(reason => console.error(reason.message))
      
  }
  
 	render(){
    //we get photo by filter=> we get the good photo by ID pass on URL
 		const photoChoosed=this.state.dataPhoto.filter(photo=>photo.id==this.props.match.params.id);
   
 		return(<article className="photo">
 				{photoChoosed.map((photo)=>(
 					<div>
 					 <h2 class="titlePhoto">{photo.title}</h2>
			      	 <img alt="" className="image" src={photo.thumbnailUrl}/>

			      	 <a href={photo.thumbnailUrl} class="lienPhoto" >Link to image</a>
			      	 <button class="btn btn-warning boutonWish" onClick={()=>this.handleClick(this, photoChoosed[0].thumbnailUrl)} > add wishlist </button>
			      	 </div>

			      ))

						}
					
			</article>);
 			
 		

 	}
}
export default Photo;
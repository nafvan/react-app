import React, {Component} from "react";
import Photo from "../Photo/Photo";
import { Link } from 'react-router-dom';
import PhotoList from "../Photo/PhotoList";
import {isoFetch} from '../../Actions/fetch';
import {  Router} from 'react-router';
import { Switch, Route } from 'react-router-dom';
import '../../css/albums.css'

/*
  Component Album=> list of albums from API
  we get the list by fetch function

*/
 	class Albums extends Component{
  constructor( props) {
      super(props)
        this.state = {
      	dataAlbum:[]

      }
   
  }

  componentWillMount() {
    const response = isoFetch( 'https://jsonplaceholder.typicode.com/albums')
   
    response
       .then(dataAlbum =>{console.log(dataAlbum); this.setState({dataAlbum}); 
       	console.log(dataAlbum)})
       .catch(reason => console.error(reason.message))
       
      
  }

  render() {

    const {title}=this.state;
  	const {dataAlbum}=this.state;
    const {userId}=this.state;
    return (
      <div className="album-List"><h2 class="titleAlbum">Album list</h2>
              <div class="row">
                {
                  dataAlbum.map(album => (
                	<div class="col-sm-4 thumbnail album well" key={album.userId}>
                    <Link to={`/albums/${album.userId}`}>{album.title}</Link>
                  </div>
                ))
                }
             </div>
      </div>);

  }

}
export default Albums;

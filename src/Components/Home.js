import React, { Component } from 'react';
import { render } from 'react-dom';
import {  Router} from 'react-router';
import Albums from './Album/Albums.js'
import PhotoList from './Photo/PhotoList';
import Photo from './Photo/Photo';
import { Switch, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Wishlist from './Wishlist/Wishlist';
import '../css/home.css';
/*

Class HOME => Contains Router of app, and the navbar of app
*/
class Home extends Component {
    render(){

        return (

			<div className="home">
       
				<nav class="navbar navbar-inverse ">
				  <div class="container-fluid">
				    <ul class="nav navbar-nav">
				      <li><Link to="/"><button class="btn btn-warning navbar-btn">Back Album List</button></Link></li>
				      <li><Link to="/wishlist"><button class="btn btn-danger navbar-btn">Wishlist</button></Link></li>
				      <li><a href="https://bitbucket.org/nafvan/react-app/src"><button class="btn btn-info navbar-btn">Go to source code </button></a> </li>
				      <li><a href="https://ponoki.fr"><button class="btn btn-primary navbar-btn">Go to portfolio </button></a> </li>
					</ul>
				  </div>
				</nav>

	        	 <Switch>
		    		 <Route path="/" exact component={Albums} />
		        	 <Route path='/wishlist' component={Wishlist} />
			         <Route path='/albums/:userId/:id' component={Photo} />
			         <Route path='/albums/:userId' component={PhotoList} />
	        	 </Switch>
       
        	</div>

        	);
    }
}


export default Home;
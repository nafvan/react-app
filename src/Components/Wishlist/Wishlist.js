import React, {Component} from "react";
import PropTypes from "prop-types";
import {getPhoto} from "../../store.js";
import '../../css/wishlist.css';

/*
  Wishlist Component=> contained photo added in wishlist
  We get this list by function getPhoto() from the store
  And we display them

*/
class Wishlist extends Component{

	
  constructor(props){
 		super(props)
 		this.state={
 			photo:[]
 		}
 	}
 	
 		
  render() {
  const listPhotos=getPhoto();
 	const liste=listPhotos.wish;
    return (
      <div className="wishlist"> 
        <h2 class="wishTitle">Wishlist</h2>
        <div class="row">
              {
              	liste.map((photo)=>
                    <div class="col-sm-3 thumbnail ">
              		    <img src={photo.add.text} />
                  </div>)
              }
    		</div>
      </div>);
  }
}
export default Wishlist;